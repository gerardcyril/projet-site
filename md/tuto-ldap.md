# LDAP

## Introduction



## Installation

~~~
firewall-cmd --permanent --add-service=ldap
firewall-cmd --reload
~~~

~~~
yum -y install openldap compat-openldap openldap-clients openldap-servers openldap-servers-sql openldap-devel
slappasswd # password root de l'admin LDAP (à noter !!)
~~~


## Configuration Serveur


fichier db.ldif

~~~
dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=mon-serveur,dc=fr

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=ldapadm,dc=mon-serveur,dc=fr

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {SSHA}j7W5sX12V4pg8YgEJKe6j9yzBB2g
~~~

ldapmodify -Y EXTERNAL  -H ldapi:/// -f db.ldif

fichier monitor.ldif 

~~~
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external, cn=auth" read by dn.base="cn=ldapadm,dc=mon-serveur,dc=fr" read by * none
~~~

~~~
ldapmodify -Y EXTERNAL  -H ldapi:/// -f monitor.ldif

cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
cp /var/lib/ldap/DB_CONFIG ~/archives

chown ldap:ldap /var/lib/ldap/*
~~~

~~~
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif 
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif
~~~

fichier base.ldif

~~~
dn: dc=mon-serveur,dc=fr
dc: mon-serveur
objectClass: top
objectClass: domain

dn: cn=ldapadm,dc=mon-serveur,dc=fr
objectClass: organizationalRole
cn: ldapadm
description: C est moi le chef ici

dn: ou=People,dc=mon-serveur,dc=fr
objectClass: organizationalUnit
ou: People

dn: ou=Group,dc=mon-serveur,dc=fr
objectClass: organizationalUnit
ou: Group
~~~

ldapadd -x -W -D "cn=ldapadm,dc=mon-serveur,dc=fr" -f base.ldif -h localhost

fichier user.ldif

~~~
dn: uid=user,ou=People,dc=mon-serveur,dc=fr
objectClass: top
objectClass: account
objectClass: posixAccount
objectClass: shadowAccount
cn: user
uid: user
uidNumber: 1006
gidNumber: 1006
homeDirectory: /home/user
loginShell: /bin/bash
gecos: user [C est moi le plus beau]
userPassword: {crypt}x
shadowLastChange: 17058
shadowMin: 0
shadowMax: 99999
shadowWarning: 7
~~~

ldapadd -x -W -D "cn=ldapadm,dc=mon-serveur,dc=fr" -f user.ldif


ldappasswd -s mypass -W -D "cn=ldapadm,dc=mon-serveur,dc=fr" -x "uid=user,ou=People,dc=mon-serveur,dc=fr"


Interroger le serveur LDAP pour vérifier la présence de l'usager qu'on a créé :
ldapsearch -x cn=user -b dc=mon-serveur,dc=fr

## Configuration Client

~~~
yum install -y openldap-clients nss-pam-ldapd
~~~

~~~
authconfig-tui

ou

authconfig --enableldap --enableldapauth --ldapserver=192.168.20.81 --ldapbasedn="dc=mon-serveur,dc=fr" --enablemkhomedir --update
~~~

... et redémarrez le service client ldap :
~~~
# systemctl restart  nslcd
~~~
Testez avec :
~~~
# getent passwd mon_user
~~~

## Démarrage du service

~~~
systemctl start slapd.service
systemctl enable slapd.service
~~~
