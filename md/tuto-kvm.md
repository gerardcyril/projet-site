# Présentation

**KVM** (Kernel-based Virtual Machine) est un hyperviseur libre de type I pour Linux qui permet la virtualisation simultanée de différents systèmes d'exploitation sur la même machine hôte. KVM est intégré dans le noyau Linux depuis la version 2.6.20 et est une instance de QEMU¹.

C'est un système optimisé pour la virtualisation de serveurs. Pour virtualiser des systèmes de type desktop, on peut lui préférer virtualbox. KVM semble en effet plus performant en consommation de processeurs mais plus lent pour l'émulation de périphérique graphique. L'utilisation d'un bureau virtualisé dans VirtualBox pourra donc laisser une meilleure impression à l'utilisateur. Vous pouvez tout de même préférer KVM pour sa meilleure compatibilité avec des systèmes d'exploitation anciens ou peu populaires.

# Sommaire

- [Installation](#install)
  - [Vérification](#verif)
  - [Installation](#install-kvm)
- [Configuration](#config)
  - [Ajout des utilisateurs aux groupes](#ajout)
  - [Création d'une image disque](#creation)
- [Mise en oeuvre](#mise-oeuvre)
  - [Installation d'un système d'exploitation](#install-os)
  - [Lancement de la VM](#lancement)

# Installation de KVM <a id="install"></a>

## Vérification <a id="verif"></a>
on va tout d'abord vérifier que votre microprocesseur permet une virtualisation avec KVM.

~~~
$ grep -E 'vmx|svm' /proc/cpuinfo &>/dev/null && echo "La virtualisation est possible." || echo "Cette machine ne permet pas d'utiliser la virtualisation avec KVM."
~~~

## Installation <a id="install-kvm"></a>
On va maintenant les paquets.

~~~
# apt-get update && apt-get install qemu-kvm libvirt-daemon-system libvirt-dev libvirt-clients
~~~

# Configuration <a id="config"></a>

## Ajout des utilisateurs aux groupes <a id="ajout"></a>
Nous allons ajouter des utilisateurs aux groupes kvm et libvirt

~~~
# adduser user kvm && adduser user libvirt
~~~

*user* est le nom de l'utilisateur qui va créer des machines virtuelles

## Création d'une image disque <a id="creation"></a>
Nous allons créer une image disque qui va nous servir de disque dur

~~~
$ qemu-img create -f qcow2 monImage 10G
~~~

*monImage* est le nom que vous donnerez à votre image
*10G* est la taille alloué au disque

# Mise en oeuvre <a id="mise-oeuvre"></a>

## Installation d'un système d'exploitation <a id="install-os"></a>
Nous allons installer le système d'exploitation grâce à un fichier image ISO

~~~
$ kvm -m 2G -cpu host monImage -cdrom NomDuFichierTéléchargé.iso -boot d
~~~

*monImage* l'image que vous aurez créer précédement
*NomDuFichierTéléchargé.iso* fichier iso servant à l'installation

**Sur un PC à distance**
ajouter ça *--vnc :0 -k fr* à la fin de la ligne
il faut que vous possèdiez vncviewver sur votre PC

## Lancement de la VM <a id="lancement"></a>
Vérifier que tap0 existe bien
Pour lancer votre VM sur notre réseau super génial.

~~~
$ kvm -net nic,model=rtl8139,vlan=0,macaddr=00:11:22:33:44:55 -net tap,vlan=0,ifname=tap0,script=no -enable-kvm -m 512 monImage
~~~

**Attention** Si vous avez des machines VirtualBox en route, KVM ne voudra pas lancer de VM.
