# Ansible

## Introduction

blabla

## Pré-requis

Avoir des VM ou des containers avec sudo et python installé (voir tuto-lxc/tuto-virtualbox)

## Installation

Installation du paquet ansible et de ses dépendances

~~~
yum install ansible
~~~

## Configuration

L'envoi d'une clef ssh à chacune des machines à monitorer

~~~
ssh-keygen

ssh-copy-id -i .ssh/id_rsa.pub formation@192.168.122.2
~~~

Création d'un fichier config dans *~/.ssh* , afin d'avoir des raccourcis pour ssh et des options pré-configurées.

~~~
vi .ssh/config
Host centos*
        user formation
        port 22
        IdentityFile ~/.ssh/id_rsa

Host debian9*
        user formation
        port 22
        IdentityFile ~/.ssh/id_rsa

Host centos
        HostName 192.168.122.2
Host centos-2
        HostName 192.168.122.3
Host debian9
        HostName 192.168.122.4
Host debian9-2
        HostName 192.168.122.5
~~~

Création des groupes dans le fichier */etc/ansible/hosts*.

~~~
vi /etc/ansible/hosts

[lxcc]
centos
centos-2

[lxcd]
debian9
debian9-2

~~~

## Utilisation

Faire un ping toutes les machines
~~~
ansible -m ping all
~~~

Installation de paquet sur les machines appartenant au groupe *lxcc*

~~~
ansible -b -m yum -a "name=figlet state=latest" lxcc -K
~~~

## Playbook

### Exemple de Playbook

Faire un ping sur toutes les machines : 

~~~
- hosts: all
  tasks:
    - name: ping
      ping:
~~~

Installation d'apache2 et démarrage du service pour les machines du groupe lxcd :

~~~
- hosts: lxcd
  become: yes
  tasks:
    - name: install apache2
      package:
        name: apache2
        state: latest
  tasks:
    - name: start apache2
      service:
        name: apache2
        state: started
~~~

Envoi du bashrc de l'utilisateur à toutes les  machines :

~~~
- hosts: all
	tasks:
	- name: envoi du bashrc
	  copy:
	    src: ~/.bashrc
	    dest: ~/.bashrc
	    mode: 0644
~~~

## Utilisation de Jinja

Récupérer les "facts" :

~~~
ansible -m setup centos | less
~~~

... vous donne la liste des variables système utilisables sur chaque hôte.
On les invoque ensute par leur nom. Ex : {{ ansible_fqdn }}

Exemple d'utilisation de variable ansible

Envoi d'un motd personnalisé

~~~
vi motd.j2
Bienvenue sur  {{ ansible_hostname }}!
~~~

~~~
vi playbook.yml
- hosts: all
  become: yes
  tasks:
    - name: Copy motd template
      template: src=motd.j2 dest=/etc/motd
~~~

Envoi d'un index.html personnalisé

~~~
vi index.html.j2
Serveurs web <br>
{% for server in groups.lxcd %}
{{ server }}  {{ hostvars[server].ansible_default_ipv4.address }} - Memoire vive disponible : free memory: {{ hostvars[server].facter_memoryfree }} <br> 
{% endfor %}
~~~

~~~
- hosts: lxcd
  become: yes
  tasks:
    - name: copy index.html
      template: src=index.html.j2 dest=/var/www/html/index.html
~~~

## Bonnes pratiques

Reduire l'affichage : https://github.com/octplane/ansible_stdout_compact_logger
Jinja fr : https://fraoustin.fr/old/jinja2.html
