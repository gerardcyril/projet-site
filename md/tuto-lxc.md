# LXC

## Introduction

blabla

## Installation

~~~
yum install epel-release
yum install lxc lxc-templates lxc-extra libcap-devel libcgroup busybox bridge-utils
~~~

~~~
systemctl enable lxc
systemctl start lxc
~~~

## Utilisation

### Création d'un container

Création d'un container en mode interactif pour sélectionner l'image du système

~~~
lxc-create -n mon-container -t download
~~~

Création d'un container

~~~
lxc-create --template download --name mon-container -- -d debian -r stretch -a amd64
~~~

### Démarrage/Arrêt du container

~~~
lxc-start -n mon-container

lxc-stop -n mon-container
~~~

### Connection au container

~~~
lxc-attack -n mon-container
~~~

### Supression d'un container

~~~
lxc-destroy -n mon-container
~~~

### Commande supplémentaire

Avoir des informations sur le container

~~~
lxc-info -n mon-container
~~~

Créer un snapshot de l'etat du container

~~~
lxc-snapshot -n mon-container
~~~

Lister les snapshots du container

~~~
lxc-snapshot -n mon-container -L
~~~

Restaurer le snapshot d'un container

~~~
lxc-snapshot -n mon-container -r nom-snapshot
~~~

Cloner un container

~~~
lxc-clone mon-container ma-copie

ou 

lxc-copy -n mon-container -N ma-copie
~~~