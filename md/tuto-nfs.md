# NFS

## Introduction

**Pré-requis :** 2 ordinateurs disposant chacun d'une interface

**Paquets à installer :** nfs

**Note :** le serveur aura comme adresse 192.168.20.161 et le poste client aura comme adresse 192.168.20.162

## Configuration coté server

On va modifier notre fichier */etc/exports*, c'est celui qui va nous permettre de définir les répertiores à partager et sous quels droits

~~~
# Syntaxe : /chemin/de/partage Client1(droits)[,Client2(droits)[...]]
# Exemples :

# Partage du répertoire /var/www/html avec l'hôte 192.168.20.162 et les droits de lectures seulement
/var/www/html 192.168.20.162(ro,sync)
 
# Partage du répertoire /home/user1 avec l'hôte 192.168.20.162 et les droits de lecture écriture et avec l'hôte 192.168.20.163 et les droits de lectures seulement
/home/user1 192.168.20.162(rw,sync) 192.168.20.163(ro,sync)
 
# Partage du répertoire /home/user2 avec tous les membres du réseau 192.168.20.1/24 en lecture simplement
/home/user2 192.168.20.1/24(ro,sync)
~~~

## Configuration optionnelle

Comme son nom l'indique le fichier */etc/hosts.allow*, va permettre de définir quels postes clients auront accès aux partages (quels postes seront autorisés à la connexion et à l'utilisation des services)

~~~
# Syntaxe : service:hote.domain.com, .domain.com
# Exemple basique (et un peu bourrin)
ALL:192.168.0.1/24
~~~

Là aussi, comme son nom l'indique le fichier */etc/hosts.deny*, va permettre de définir quels postes clients n'auront pas accès aux partages (quels postes seront interdis à la connexion et à l'utilisation des services)

~~~
# Syntaxe : service:hote.domain.com, .domain.com
# Exemple basique (et un peu bourrin)
ALL:PARANOID
~~~

## Démarrage du service

~~~
systemctl restart nfs
~~~

## Configuration coté client

~~~
mount -t nfs 192.168.20.161:/var/www/html /var/www/html
mount -t nfs 192.168.20.161:/home/user1 /home/user1
mount -t nfs 192.168.20.161:/home/user2 /home/partages/user2
~~~
