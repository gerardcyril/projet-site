# DHCP

## Introduction

blabla

## Installation

blabla

~~~
yum install dhcp dhcp-utils
~~~

## Configuration

Pour configurer notre service DHCP, on va modifier le fichier */etc/dhcp/dhcpd.conf*.  

~~~
default-lease-time 86400; # 24h
max-lease-time 172800; # 48h

# Declaration du reseau
subnet 192.168.20.160 netmask 255.255.255.240 {
	range				192.168.20.162 192.168.20.174;
	option subnet-mask		255.255.255.0;
	option domain-name-servers	192.168.20.254;
	option routers			192.168.20.254;

	
	host debian1 {
		hardware ethernet 08:00:27:DC:E6:DC;
		fixed-address 192.168.20.162;
		option host-name debian1;
	}
}
~~~

## Démarrage du service

On mettre notre service au démarrage de la machine et le démarrer.

~~~
systemctl enable dhcp
systemctl start dhcp
~~~