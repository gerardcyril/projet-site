# Présentation <a id="presentation"></a>

Les logiciels de gestion de versions sont utilisés principalement par les développeurs ; ce sont donc bel et bien des outils pour geeks. En effet, ils sont quasi exclusivement utilisés pour gérer des codes sources, car ils sont capables de suivre l’évolution d’un fichier texte ligne de code par ligne de code. ;) 
Ces logiciels sont fortement conseillés pour gérer un projet informatique.

S’ils s’arrêtaient à cela, ce ne seraient que de vulgaires outils de backup (sauvegarde). Cependant, ils proposent de nombreuses fonctionnalités qui vont vraiment vous être utiles tout au long de l’évolution de votre projet informatique :

- ils retiennent qui a effectué chaque modification de chaque fichier et pourquoi. Ils sont par conséquent capables de dire qui a écrit chaque ligne de code de chaque fichier et dans quel but
- si deux personnes travaillent simultanément sur un même fichier, ils sont capables d’assembler (de fusionner) leurs modifications et d’éviter que le travail d’une de ces personnes ne soit écrasé

Ces logiciels ont donc par conséquent deux utilités principales :

- suivre l’évolution d’un code source, pour retenir les modifications effectuées sur chaque fichier et être ainsi capable de revenir en arrière en cas de problème
- travailler à plusieurs, sans risquer de se marcher sur les pieds. Si deux personnes modifient un même fichier en même temps, leurs modifications doivent pouvoir être fusionnées sans perte d’information

# Sommaire 

- [Schéma](#schema)
- [Commandes](#commandes)
  - [Créer un nouveau dépôt](#creer-depot)
  - [Cloner un dépôt](#cloner-depot)
  - [Arbres](#arbres)
  - [Ajouter et Valider](#ajouter-valider)
  - [Envoyer des changements](#envoyer-changements)
  - [Mettre à jour et fusionner](#mettre-fusionner)
  - [Tags](#tags)
  - [Corriger une erreur](#corriger-erreurs)
- [Sources](#Sources)

# Schéma <a id="schema"></a>

On utilise très souvent un serveur qui sert de point de rencontre entre les développeurs. Le serveur connaît l’historique des modifications et permet l’échange d’informations entre les développeurs, qui eux possèdent également l’historique des modifications.

![schema](img/tuto/git01.png)

# Installer Git

**Sous Linux** : apt install git

**Sous Windows** : https://gitforwindows.org

**Sous Mac** : https://git-scm.com/download/mac

# Commandes <a id="commandes"></a>

## Créer un nouveau dépôt <a id="creer-depot"></a>

créez un nouveau dossier, ouvrez le et exécutez la commande suivante pour créer un nouveau dépôt.

~~~
git init
~~~

## Cloner un dépôt <a id="cloner-depot"></a>

Créez une copie de votre dépôt local en exécutant la commande

~~~
git clone /path/to/repository
~~~

## Arbres <a id="arbres"></a>
votre dépôt local est composé de trois "arbres" gérés par git :
- espace de travail qui contient réellement vos fichiers
- Index qui joue un rôle d'espace de transit pour vos fichiers
- HEAD qui pointe vers la dernière validation que vous ayez faite

![arbre](img/tuto/git02.png)

## Ajouter & valider <a id="ajouter-valider"></a>

Vous pouvez proposer un changement (l'ajouter à l'**Index**) en exécutant les commandes

~~~
git add <filename>
git add *
~~~

C'est la première étape dans un workflow git basique. Pour valider ces changements, utilisez

~~~
git commit -m "Message de validation"
~~~

Le fichier est donc ajouté au HEAD, mais pas encore dans votre dépôt distant.

## Envoyer des changements <a id="envoyer-changements"></a>

Vos changements sont maintenant dans le **HEAD** de la copie de votre dépôt local. Pour les envoyer à votre dépôt distant, exécutez la commande

~~~
git push origin master
~~~

Remplacez *master* par la branche dans laquelle vous souhaitez envoyer vos changements. 

Si vous n'avez pas cloné votre dépôt existant et voulez le connecter à votre dépôt sur un serveur distant, vous devez l'ajouter avec

~~~
git remote add origin <server>
~~~

Maintenant, vous pouvez envoyer vos changements vers le serveur distant sélectionné

## Mettre à jour & fusionner <a id="mettre-fusionner"></a>
pour mettre à jour votre dépôt local vers les dernières validations, exécutez la commande

~~~
git pull
~~~

dans votre espace de travail pour récupérer et fusionner les changements distants.

Malheureusement, ça n'est pas toujours possible et résulte par des conflits. Vous devez alors régler ces conflits manuellement en éditant les fichiers indiqués par git. Après l'avoir fait, vous devez les marquer comme fusionnés avec

~~~
git add <filename>
~~~

## Tags <a id="tags"></a>

Il est recommandé de créer des tags pour les releases de programmes. Vous pouvez créer un tag nommé 1.0.0 en exécutant la commande

~~~
git tag 1.0.0 1b2e1d63ff
~~~

le *1b2e1d63ff* désigne les 10 premiers caractères de l'identifiant du changement que vous voulez référencer avec ce tag. Vous pouvez obtenir cet identifiant avec

~~~
git log
~~~

Pour supprimer un tag

~~~
git tag -d NomTag
~~~

## Corriger une erreur <a id="corriger-erreurs"></a>

**Modifier le dernier message de commit**

~~~
git commit --amend
~~~

**Annuler le dernier commit**

~~~
git reset HEAD
~~~

Pour indiquer à quel commit on souhaite revenir, il existe plusieurs notations :

- HEAD : dernier commit
- HEAD^ : avant-dernier commit
- HEAD^^ : avant-avant-dernier commit
- HEAD~2 : avant-avant-dernier commit (notation équivalente)
- d6d9892386 : indique un numéro de commit précis

**Annuler les modifications d’un fichier avant un commit**

~~~
git checkout nomfichier
~~~

Il est possible de retirer un fichier qui avait été ajouté pour être « commité » en procédant comme suit :

~~~
git reset HEAD -- fichier_a_supprimer
~~~

# Sources <a id="Sources"></a>

- [Git Community](https://book.git-scm.com)
- [Like a Git](http://think-like-a-git.net)
- [Visual Git](http://marklodato.github.io/visual-git-guide/index-en.html)
- [Github Help](https://help.github.com)
- [GitLab help](https://gitlab.com/help)