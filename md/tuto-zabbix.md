# Zabbix


## Introduction

## Installation

### Installation du service Web

Apache
~~~
yum -y install httpd
systemctl start httpd
systemctl enable httpd
~~~

PHP
~~~
yum -y install epel-release
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

yum -y install mod_php72w php72w-cli php72w-common php72w-devel php72w-pear php72w-gd php72w-mbstring php72w-mysql php72w-xml php72w-bcmath
~~~

~~~
vim /etc/php.ini
	max_execution_time = 600
	max_input_time = 600
	memory_limit = 256M
	post_max_size = 32M
	upload_max_filesize = 16M
	date.timezone = Europe/Paris

systemctl restart httpd
~~~

MariaDB
~~~
yum -y install mariadb-server
systemctl start mariadb
systemctl enable mariadb

mysql_secure_installation
~~~

## Configuration serveur

BDD pour Zabbix

~~~
mysql -u root -p
create database zabbix;
grant all privileges on zabbix.* to zabbix@'localhost' identified by 'mon_Super_mot_de_Passe';
grant all privileges on zabbix.* to zabbix@'%' identified by 'mon_Super_mot_de_Passe';
flush privileges;
~~~

Zabbix
~~~
yum -y install http://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-3.5-1.el7.noarch.rpm
yum -y install zabbix-get zabbix-server-mysql zabbix-web-mysql zabbix-agent

cd /usr/share/doc/zabbix-server-mysql-4.0.0/
gunzip create.sql.gz
mysql -u root -p zabbix < create.sql
~~~

~~~
vim /etc/zabbix/zabbix_server.conf
DBHost=localhost
DBPassword=mon_Super_mot_de_Passe
~~~

~~~
systemctl restart httpd
systemctl start zabbix-server
systemctl enable zabbix-server
systemctl status zabbix-server -l
~~~


Règle firewall
~~~
firewall-cmd --add-service={http,https} --permanent
firewall-cmd --add-port={10051/tcp,10050/tcp} --permanent
firewall-cmd --reload
firewall-cmd --list-all
~~~

On redémarre tout avant de passer à l'interface graphique :

~~~
systemctl restart zabbix-server
systemctl restart zabbix-agent
systemctl restart httpd
~~~

Puis dans un navigateur :
- http://mon_IP/zabbix/

## Configuration client

~~~
vim /etc/zabbix/zabbix_agentd.conf
Server=127.0.0.1
ServerActive=127.0.0.1
Hostname=Mon_super_Hostname
~~~

~~~
Server=127.0.0.1
ServerActive=127.0.0.1
Hostname=Mon_super_Hostname
~~~