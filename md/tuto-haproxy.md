# HAProxy

Ce tutoriel explique comment configurer un équilibreur de charge à deux noeuds avec HAProxy sous CentOS 7. L'équilibreur de charge se trouve entre l'utilisateur et deux (ou plus) serveurs web Apache qui contiennent le même contenu.  Si l'une d'entre elles est en panne, toutes les demandes seront automatiquement redirigées vers le serveur dorsal restant, ce qui signifie que les utilisateurs ne remarqueront aucune interruption du service.

## Introduction

**HAProxy** est une solution gratuite, très rapide et fiable offrant une haute disponibilité, un équilibrage de charge et un proxy pour les applications **TCP** et **HTTP**. Il est particulièrement adapté aux sites Web à très fort trafic et permet d'accéder à un grand nombre des sites les plus visités dans le monde. Au fil des ans, il est devenu l'équilibreur de charge open source standard, et est maintenant livré avec la plupart des distributions Linux classiques.

## Pré-requis

- Deux machines où le répertoire */var/www/html* a été synchronisé que nous appelerons **httpd1** et **httpd2**
- Une machine qui nous servira de serveur proxy que nous appelerons **haproxy**

Vérifier bien que vos serveur sont accessible sur le port 80.


## Installation

On va mettre à jour notre machine

~~~
yum update
yum install haproxy
~~~

## Configuration de HAProxy en http

Pour configurer HAProxy, il faudra ajouter notre configuration http au fichier */etc/haproxy/haproxy.cfg*.

~~~
# [HTTP Site Configuration]
listen  http_web 192.168.20.98:80
        mode http
        balance roundrobin
        option httpchk
        option forwardfor
        server server1 192.168.10.99:80 weight 1 maxconn 512 check
        server server2 192.168.10.100:80 weight 1 maxconn 512 check
~~~

## Configuration de HAProxy en https

N'oubliez pas de définir un nom de domaine pour votre site soit dans votre fichier */etc/hosts*, soit dans votre DNS, sinon votre certificat ne sera pas valide.

Ajout d'un nom de domaine pour notre site

~~~
192.168.20.98	www.mon-site.fr
~~~

### Création d'un certificat

Nous allons tout d'abord nous créer un certificat qui va nous servir lors de la configuration de HAProxy

~~~
# Générez une clef privée :
openssl genrsa -out ca.key 2048

# Générez le CSR 
openssl req -new -key ca.key -out ca.csr

# Générez la clef auto-signée : 
openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

# Copiez les fichiers aux endroits appropriés :

mv ca.crt /etc/pki/tls/certs/
mv ca.key /etc/pki/tls/private/
mv ca.csr /etc/pki/tls/private/
~~~

Concaténons les fichiers .crt et .key dans un fichier .pem afin que HAProxy puisse prendre en compte le certificat:

~~~
cat ca.crt ca.key > ca.pem
mv ca.pem /etc/pki/tls/private/
~~~

Editons le fichier */etc/haproxy/haproxy.cfg* comme suit:

~~~
global
	log 127.0.0.1	local2
	maxconn 10000
	tune.ssl.default-dh-param 2048
	user haproxy
	user haproxy
	daemon

defaults
	log global
	option forwardfor
	option http-server-close
	option httplog
	option redispatch
	retries 3
	timeout connect 3s
	timeout client 15s
	timeout server 5s
	# stats
	stats enable
   	stats uri /stats
   	stats realm Haproxy\ Statistics
   	stats auth user:password

frontend examplecom_redirect_https
#	bind *:80
	bind *:443 ssl crt /etc/pki/tls/private/
	redirect scheme https code 301 if !{ ssl_fc }
	mode http
	reqadd X-Forwarded-Proto:\ http
	reqadd X-Forwarded-Proto:\ https
	default_backend examplecom_http_backend

backend examplecom_http_backend
	mode http
	balance roundrobin
	server web1 192.168.20.99:80 check
	server web2 192.168.20.100:80 check
~~~

Testons notre configuration

~~~
haproxy -f /etc/haproxy/haproxy.cfg -c
[ALERT] 239/165938 (16574) : parsing [/etc/haproxy/haproxy.cfg:6] : user/uid already specified. Continuing.
[WARNING] 239/165938 (16574) : parsing [/etc/haproxy/haproxy.cfg:30] : a 'reqadd' rule placed after a 'redirect' rule will still be processed before.
[WARNING] 239/165938 (16574) : parsing [/etc/haproxy/haproxy.cfg:31] : a 'reqadd' rule placed after a 'redirect' rule will still be processed before.
Configuration file is valid
~~~

On voit que la configuration est valide.

### Démarrer le service

~~~
systemctl start haproxy
systemctl enable haproxy
~~~

Et maintenant vous pouvez accedez à votre site depuis votre serveur haproxy, soit avec son adresse IP : 192.168.20.98, soit avec le nom de domaine que vous aurez défini pour celui-ci www.mon-site.fr