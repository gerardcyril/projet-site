# VirtualHost

## Introduction

blabla

## Pré-requis

blabla, avoir un serveur avec un repertoire qui marche

## Configuration en HTTP

On va editer notre fichier /etc/httpd/conf/httpd.conf

~~~
<VirtualHost *:80>
   DocumentRoot "/var/www/html/"
   ServerName www.mon-site.fr
   ServerAlias mon-site.fr
</VirtualHost>

<VirtualHost *:80>
    DocumentRoot "/var/www/html/mon-blog"
    ServerName blog.mon-site.fr
    ServerAlias blog.mon-site.fr
	ErrorLog /var/www/html/mon-blog/error.log
</VirtualHost>
~~~

##Configuration en HTTPS

On va tout d'abord permettre à notre serveur d'écouter sur le port 443 et télécharger le paquet necessaire pour créer notre certificat

~~~
vi /etc/httpd/conf/httpd.conf
Listen 80 443

yum install mod_ssl openssl
~~~

On va ensuite se créer un certificat 

~~~
# Générez une clef privée :
openssl genrsa -out ca.key 2048 

# Générez le CSR 
openssl req -new -key ca.key -out ca.csr

# Générez la clef auto-signée : 
openssl x509 -req -days 365 -in ca.csr -signkey ca.key -out ca.crt

# Copiez les fichiers aux endroits appropriés :

cp ca.crt /etc/pki/tls/certs/
cp ca.key /etc/pki/tls/private/
cp ca.csr /etc/pki/tls/private/
~~~

On peut maintenant créer notre VirtualHost dans notre fichier *httpd.conf*

~~~
<VirtualHost *:443>
        SSLEngine on
        SSLCertificateFile /etc/pki/tls/certs/ca.crt
        SSLCertificateKeyFile /etc/pki/tls/private/ca.key
        ServerName www.mon-site.fr
        ServerAlias mon-site.fr
        DocumentRoot /var/www/html/
</VirtualHost>

<VirtualHost *:443>
	SSLEngine on
        SSLCertificateFile /etc/pki/tls/certs/ca.crt
        SSLCertificateKeyFile /etc/pki/tls/private/ca.key
        ServerName wiki.mon-site.fr
        ServerAlias wiki.mon-site.fr
        DocumentRoot /var/www/html/wiki/

</VirtualHost>
~~~

## Démarrage du service

On va redémarrer notre serveur 

~~~
apachectl configtest
Syntax OK

systemctl reload httpd
~~~

Vous pouvez maintenant vous rendre dnas un repertoire en utilisant l'alias que vous aurez précisé dans le fichier *httpd.conf*