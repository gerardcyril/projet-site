# Installation et configuration LDAP

**Commandes utiles :**
Pour les génération de mot de passe crypté

~~~
slappasswd
~~~

Faire des recherche sur l’annuaire

~~~
ldapsearch -x -h localhost -b dc=mabase,dc=org -D cn=admin,dc=mabase,dc=org -W
~~~

Supprimer le contenu de l’annuaire jusqu'à la racine

~~~
ldapdelete -r -c -Y EXTERNAL -H ldapi:/// dc=mabase,dc=org
~~~

## Installation du serveur LDAP

1. Installation du paquet

~~~
apt-get install sldap
~~~

2. Création d'un répertoire de travail

~~~
mkdir -p /srv/ldap/iut
chmod -R /srv/ldap/iut 755
~~~

## Création de notre base

Voici la commande pour ajouter les fichiers que nous allons créer à la base

~~~
ldapadd -c -Y EXTERNAL -H ldapi:/// -f /srv/ldap/iut/mon-fichier.ldif
~~~

1. Création d'une base
Pour cela, il faudra indiquer dans quel répertoire on veut travailler et quel sera le mot de passe admin de notre base.

~~~
vi mabase.ldif
~~~

Le contenu du fichier :

~~~
dn: olcDatabase=mdb,cn=config 	#définition de l’objet
objectClass: olcDatabaseConfig 	#désigne la classe associé
objectClass: olcMdbConfig 		#définition de la classe du moteur de stockage
olcDatabase: mdb 				#précise le moteur de stockage
olcDbDirectory: /srv/ldap/iut 	#répertoire de l’annuaire
olcSuffix: dc=mabase,dc=org 	#Suffixe de la base
olcRootDN: cn=admin,dc=mabase,dc=org #le DN du root
olcRootPW: {SSHA}0sKqRIO3F36E04c9wjG0K7qgPrN1hcbD #mot de passe root
~~~

2. Ajout de l'objet racine à la base
On va ensuite créer le premier objet de notre base, celui qui va servir de base à notre annuaire. Cette racine va nous permettre de faire des modification et des recherches sur notre annuaire.

~~~
vi mabaseroot.ldif
~~~

Le contenu du fichier :

~~~
dn: dc=mabase,dc=org #Définition de la racine
objectClass: dcObject #La classe de l’objet
objectClass: organization #La classe de l’objet
dc: mabase #Définition du dc mabase
o: mabase.org #Définition de la racine
~~~

3. Ajout de l'utilisateur administrateur
On va maintenant créer un administrateur à notre annuaire pour qu’il puisse ainsi gérer cet annuaire.

~~~
vi admin.ldif
~~~

Le contenu du fichier :

~~~
dn: cn=admin,dc=mabase,dc=org
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: Administrator
userPassword: {SSHA}MBNo5UcolFvFGqV97PQCP7aFWC0JwSp4
~~~

## Configuration de notre base
1. Conception du modèle de nommage
Le modèle de nommage a pour but de définir la façon selon laquelle les objets de l'annuaire sont nommés et classés.
Ainsi les objets LDAP sont classés hiérarchiquement et possèdent un espace de nom homogène. Cela signifie que d'un annuaire à un autre, un objet de la même classe possèdera le même nom afin de garantir une compatibilité entre les annuaires.

2. Création du modèle de nommage
Suite au modèle de nommage que nous avons définis précédemment, il nous faut donc définir des fichiers à créer. Nous allons décidé de découper notre arbre en plusieurs fichiers.

~~~
vi tronc.ldif
~~~

Le contenu du fichier :

~~~
dn: ou=INFO,dc=mabase,dc=org 	#nom absolu
objectClass: organizationalUnit #la classe du noeud
ou: INFO 						#nom de notre ou

dn: ou=BIO,dc=mabase,dc=org
objectClass: organizationalUnit
ou: BIO

dn: ou=GEII,dc=mabase,dc=org
objectClass: organizationalUnit
ou: GEII
~~~

~~~
vi premierebranche.ldif
~~~

Le contenu du fichier :

~~~
dn: ou=Formation,ou=INFO,dc=mabase,dc=org
objectClass: organizationalUnit
ou: Formation

dn: ou=DUT,ou=Formation,ou=INFO,dc=mabase,dc=org
objectClass: organizationalUnit
ou: DUT

dn: ou=Licence Professionnelle,ou=Formation,ou=INFO,dc=mabase,dc=org
objectClass: organizationalUnit
ou: LP
~~~

~~~
vi personnels.ldif
~~~

Le contenu du fichier :

~~~
dn: ou=Personnel,ou=INFO,dc=mabase,dc=org
objectClass: organizationalUnit
ou: Personnel

dn: ou=Professeur des
universites,ou=Personnel,ou=INFO,dc=mabase,dc=org
objectClass: organizationalUnit
ou: Professeur des universites

dn: ou=Maitre de conferences,ou=Personnel,ou=INFO,dc=mabase,dc=org
objectClass: organizationalUnit
ou : Maitre de conferences
~~~

3. Ajout d'utilisateur
Maintenant que nous avons notre modèle fonctionnel, nous pouvons ajouter des utilisateurs, il nous faut donc créer de nouveaux fichiers.
Ici notre choix se porte sur une classe InetOrgPerson​, qui est une classe qui comporte tous les attributs d’une personne ( nom, prénom, mail, et mot de passe,...).

~~~
vi utilisateurs.ldif
~~~

Le contenu du fichier :

~~~
dn: cn=Prenom Nom,ou=DUT,ou=Formation,ou=BIO,dc=mabase,dc=org
objectClass: inetOrgPerson
cn: Prenom Nom
sn: nomPrenom
givenName: Prenom
mail: nom.prenom@mabase.fr
userPassword: {SSHA}oJGwDgI50hXGkseyWqUbePJzhIhKzd+q
~~~

4. Ajout des droits à nos utilisateurs
Pour finir, il reste à ajouter les droits de nos utilisateurs, nous avons donc un dernier fichier à créer droits.ldif

~~~
vi droits.ldif
~~~

Le contenu du fichier :

~~~
dn: olcDatabase={2}mdb,cn=config # Chemin de modification des droits
changeType: modify # indique que l’on modifie la base
add: olcAccess # indique ce que l’on modifie
olcAccess: to attrs=userPassword by dn.exact="cn=admin,dc=mabase,dc=org"
write by self write by anonymous auth by * none
olcAccess: to * by dn.exact="cn=admin,dc=mabase,dc=org" manage by * read
~~~
