# DNS

## Introduction

Adresse de ma machine 192.168.20.161/24

## Installation

~~~
yum install bind bind-utils
~~~

## Configuration

On va tout d'abord configurer notre fichier */etc/named.conf*

~~~
options {
	listen-on port 53 { 127.0.0.1; 192.168.20.161; };
	listen-on-v6 port 53 { ::1; };
	directory 	"/var/named";
	dump-file 	"/var/named/data/cache_dump.db";
	statistics-file "/var/named/data/named_stats.txt";
	memstatistics-file "/var/named/data/named_mem_stats.txt";
	allow-query     { 192.168.20.0/24; };
	
	forwarders { 
	192.168.20.254;
	8.8.8.8;
	};

	recursion yes;

	dnssec-enable yes;
	dnssec-validation yes;
	bindkeys-file "/etc/named.iscdlv.key";
	managed-keys-directory "/var/named/dynamic";
	pid-file "/run/named/named.pid";
	session-keyfile "/run/named/session.key";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "." IN {
	type hint;
	file "named.ca";
}

zone "mon-site.fr" IN {
	type master;
	file "mon-site.fr.zone";
	allow-update { none; };
};

zone "20.168.192.in-addr.arpa" IN {
	type master;
	file "mon-site.fr.reverse";
	allow-update { none; };
};
~~~

On va ensuite créer un fichier mon-site.fr.zone

~~~
$TTL 3H
@      IN       SOA       mon-site.fr. root.mon-site.fr. (
                            0     ; serial
                            1D    ; refresh
                            1H    ; retry
                            1W    ; expire
                            3H )
@      IN       NS        mon-site.fr.
@      IN       A         192.168.20.161
cyril  IN       A         192.168.20.161
www    IN       CNAME     mon-site.fr.
~~~

Nous allons aussi créer notre reverse

~~~
$TTL 3H
@      IN       SOA       mon-site.fr. root.mon-site.fr. (
                          0     ; serial
                          1D    ; refresh
                          1H    ; retry
                          1W    ; expire
                          3H )
@      IN       NS        mon-site.fr.
@      IN       PTR       mon-site.fr.
161    IN       PTR       mon-site.fr.

~~~


## Démarrage du service

~~~
systemctl start named

systemctl status -l named

~~~ 


Redémarrez le serveur à chaque changement dans les fichiers de conf;
