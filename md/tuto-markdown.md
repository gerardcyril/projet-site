# Présentation

Markdown est un système d’édition et de formatage de texte ; c’est à la fois une syntaxe, un script de conversion texte → HTML et un format de fichier. Il est couramment utilisé pour les fichiers de documentation d’un projet ou d’un jeu de données -souvent nommé README.md. Il est stocké au format texte classique et est plus léger que sa version interprétée puisqu’il ne contient pas les balises html.

La philosophie du système veut que le texte écrit soit lisible sans interpréteur particulier en mode texte. Il est léger et épuré de l’essentiel de la verbosité d’un language balisé. Les éléments de syntaxe sont des caractères de ponctuation qui font sens visuellement même non convertis. Une fois converti, le navigateur web (qui joue alors le rôle d’interpréteur) en rendra la lecture plus claire.

Les fichiers sont généralement enregistrés avec l’extension .md (ou .markdown ) pour indiquer aux interpréteur la nature du texte qu’il vont lire ; mais ça n’a rien d’obligatoire.

Comme le résultat sera exporté en HMTL, vous pouvez tout à fait introduire directement des balises HTML dans votre texte ; mais celui-ci deviendra moins lisible et ne pourra plus être édité par quelqu’un ne maîtrisant pas le HTML. Attention, le formatage markdown ne sera pas appliqué à l’intérieur de ces balises.

# Sommaire

- [La syntaxe de Markdown](#syntaxe)
- [Titres](#titre)
- [Liens](#lien)
- [Images](#image)
- [Code](#code)
- [Listes](#liste)
- [Tableaux](#tableau)
- [Autres](#autre)
- [Exporter](#exporter)

# La syntaxe de Markdown <a id="syntaxe"></a>

\*Italique\* = *Italique*

\*\*Gras\*\* = **Gras**

\~\~Barré\~\~ = ~~Barré~~

\`Surligner\` = `Surligner`

\`\~~\*\*\_Italic, Gras, Barré et Surligner\_\*\*\~~\` = ~~**_`Italic, Gras, Barré et Surligner`_**~~


# Titres <a id="titre"></a>

**Ecriture :**

\# Titre 1
\#\# Titre 2
\#\#\# Titre 3

**Résultat :**

# Titre 1
## Titre 2
### Titre 3

# Liens <a id="lien"></a>

\[lien\]\(monsite.com\) = [lien](monsite.com)

**Liens internes**

\#\#\# Titre 3 <\a id="titre3"></\a>

\[lien\]\(\#titre3\) = [lien](#titre3)

**Liens de référence**

\[mon-lien\]: monsite.com *cette ligne ne sera pas visible sur la page HTML*

[mon-lien]: monsite.com

\[lien\]\[mon-lien\] = [lien][mon-lien]

# Images <a id="image"></a>

\!\[nom-image\]\(mon-image\)

![mon-image](https://markdown-here.com/img/icon256.png)

# Code <a id="code"></a>

**Ecriture :**

\~~~
mon code
\~~~

ou

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mon code

**Résultat :**

~~~
mon code
~~~

# Listes <a id="liste"></a>

**Ecriture :**

\- Puce
\- Puce
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\- Sous-Puce
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\- Sous-Puce
\- Puce

1.&nbsp;Puce 1
2.&nbsp;Puce 2
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.&nbsp;Sous-Puce 1
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.&nbsp;Sous-Puce 2
3.&nbsp;Puce 3

**Résultat :**

- Puce
- Puce
	- Sous-Puce
	- Sous-Puce
- Puce

1. Puce 1
2. Puce 2
 	1. Sous-Puce 1
 	2. Sous-Puce 2
3. Puce 3

# Tableaux <a id="tableau"></a>

**Ecriture :**

\| C1-L1|C2-L1|C3-L1|
\| :-----: | :-----: | :-----: |
\| C1-L2 | C2-L2 | C3-L2 |

**résultat :**

| C1-L1|C2-L1|C3-L1|
| :-: | :-: | :-: |
| C1-L2 | C2-L2 | C3-L2 |

# Autres <a id="autre"></a>

**barre de séparation :**

toto
\*\*\* ou \-\-\-
titi

toto
***
titi

**Mettre un commentaire :**

toto
<\!-- texte en commentaire -->
titi

toto
<!-- texte en commentaire -->
titi

# Exporter <a id="exporter"></a>

Si vous êtes sous **Linux**, pandoc est la solution pour convertir vos fichiers markdown.

Pour **Windows**, soit vous utilisez la solution de Linux grâce à une VM, soit vous vous rendez sur le site stackedit.io où vous pourrez exporter vos fichiers markdown en HTML. Sinon vous avez le site hackmd.io qui permet lui aussi de convertir en HTML et pleins d'autres formats.