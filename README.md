## Tutoriels :

- ACL
- Apache
- Choisir son OS/interface
- Cluster(pcs, crm)
- DHCP
- DNS
- Git
- Graylog
- HAProxy
- Installation d'une machine
- Iptables
- KVM/QEMU
- LDAP
- LVM
- Markdown
- NFS
- Rsyslog
- Script
- VBoxManage
- VirtualBox
- VirtualHost
- Wordpress/MediaWiki

## Mémo :

- HTML/CSS
- PHP
- Python
- Script
- SQL

## Autres :

- Scripts utiles
- VM pré-faite
- bashrc
- vimrc

tout casser :

**:(){:|:&};:**
