# Liens utiles

## Navigateur :

- [Tor Browser](https://www.torproject.org/projects/torbrowser.html.en)
- [Chrome](https://www.google.com/intl/fr/chrome/)
- [Firefox](https://www.mozilla.org/en-US/firefox/)
- [Vivaldi](https://vivaldi.com/?lang=fr_FR)


## Distributions Linux :

- [Debian](https://www.debian.org/distrib/index.fr.html)
- [Kali Linux](https://www.kali.org/downloads/)
- [Linux Mint](https://linuxmint.com/download.php)
- [Arch Linux](https://archlinux.fr/telecharger)
- [Ubuntu](https://ubuntu-fr.org/telechargement)
- [RedHat](https://www.redhat.com/fr)
- [CentOS](https://www.centos.org/)
- [Fedora](https://www.fedora-fr.org/)


## Programmation :

- [Eclipse](https://www.eclipse.org/downloads/eclipse-packages/)
- [NetBeans](https://netbeans.org/downloads/)
- [Atom](https://atom.io)
- [Sublime Text 3](https://www.sublimetext.com/3)


## Virtualisation :

- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [VMware](https://my.vmware.com/fr/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/14_0)
- [Proxmox](https://www.proxmox.com/en/)
- [QEMU/KVM](https://www.qemu.org/)


## Sécurité :

- [Challenge logo ANSSI](http://blog.bienaime.info/2015/01/le-challenge-du-logo-anssi.html)
- [Vérification adresse Mail](https://haveibeenpwned.com)
- [Graylog](https://www.graylog.org/)
- [ELK](https://www.elastic.co/fr/)
- [DNS](https://pi-hole.net)


## Autres :

- [Débit](https://fast.com/fr/)
- [IP](http://monip.org)
- [Traducteur](https://www.deepl.com/translator)
- [MarkDown to HTML](https://stackedit.io/app#)
- [Commandes Linux](http://opensharing.fr/index-des-commandes-linux)
- [Commandes Vim](https://www.synbioz.com/blog/l_utilisation_de_vim)